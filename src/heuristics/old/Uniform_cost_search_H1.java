/* 
 * Copyright (C) 2010-2017 Enrico Scala. Contact: enricos83@gmail.com.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package heuristics.old;

import conditions.Comparison;
import conditions.Conditions;
import conditions.NotCond;
import conditions.Predicate;
import expressions.ExtendedNormExpression;
import expressions.NumEffect;
import extraUtils.Utils;
import heuristics.Heuristic;
import static java.lang.Float.MAX_VALUE;
import static java.lang.System.out;
import java.util.ArrayList;
import java.util.Collection;
import static java.util.Collections.nCopies;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.logging.Level;
import static java.util.logging.Level.SEVERE;
import java.util.logging.Logger;
import org.jgrapht.util.FibonacciHeap;
import org.jgrapht.util.FibonacciHeapNode;
import problem.GroundAction;
import problem.State;
import static java.util.logging.Logger.getLogger;
import static java.util.logging.Logger.getLogger;
import problem.GroundEvent;
import problem.GroundProcess;
import static java.util.logging.Logger.getLogger;
import static java.util.logging.Logger.getLogger;
import static java.util.logging.Logger.getLogger;
import static java.util.logging.Logger.getLogger;
import static java.util.logging.Logger.getLogger;
import static java.util.logging.Logger.getLogger;

/**
 *
 * @author enrico
 */
public class Uniform_cost_search_H1 extends Heuristic {

    protected HashMap<Integer, LinkedHashSet<Conditions>> achieve;
    protected HashMap<Integer, LinkedHashSet<GroundAction>> achievers_inverted;
    protected HashMap<Integer, LinkedHashSet<Comparison>> interact_with;
    protected HashMap<Integer, LinkedHashSet<Comparison>> possible_achievers;
    protected HashMap<Integer, LinkedHashSet<GroundAction>> possible_achievers_inverted;
    protected HashMap<Integer, LinkedHashSet<GroundAction>> precondition_mapping;
    protected HashMap<Conditions, Boolean> redundant_constraints;

    protected boolean reacheability_setting;
    private boolean all_paths = false;
    protected ArrayList<Integer> dist;

    public Uniform_cost_search_H1(Conditions G, Set<GroundAction> A) {
        super(G, A);
    }

    /**
     *
     * @param goals
     * @param actions
     * @param processesSet
     */
    public Uniform_cost_search_H1(Conditions G, Set A, Set processesSet) {
        super(G, A, processesSet);
    }

    public Uniform_cost_search_H1(Conditions G, Set A, Set processesSet, Set events) {
        super(G, A, processesSet, events);
    }

    @Override
    public Float setup(State s) {

        build_integer_representation();
        identify_complex_conditions(A);
        try {
            generate_achievers();
        } catch (Exception ex) {
            Logger.getLogger(Uniform_cost_search_H1.class.getName()).log(Level.SEVERE, null, ex);
        }
        reacheability_setting = true;
        Utils.dbg_print(debug, "Reachability Analysis Started");
        Float ret = compute_estimate(s);
        Utils.dbg_print(debug, "Reachability Analysis Terminated");
        reacheability_setting = false;
        sat_test_within_cost = false; //don't need to recheck precondition sat for each state. It is done in the beginning for every possible condition
        out.println("Hard Conditions: " + this.complex_conditions);
        out.println("Simple Conditions: " + (this.all_conditions.size() - this.complex_conditions));
        return ret;
    }

    @Override
    public Float compute_estimate(State s) {
        //PriorityQueue<ConditionsNode> q = new PriorityQueue();
        FibonacciHeap<Conditions> q = new FibonacciHeap();

//        relaxed_plan_actions = new LinkedHashSet();
        //setting up the initial values
        ArrayList<Boolean> closed = new ArrayList<>(nCopies(all_conditions.size() + 1, false));
        ArrayList<Float> dist = new ArrayList<>(nCopies(all_conditions.size() + 1, MAX_VALUE));
        ArrayList<Boolean> open_list = new ArrayList<>(nCopies(all_conditions.size() + 1, false));
        HashMap<Integer, FibonacciHeapNode> cond_to_entry = new HashMap();
        for (Conditions c : all_conditions) {
            if (s.satisfy(c)) {
                FibonacciHeapNode node = new FibonacciHeapNode(c);
                q.insert(node, 0);
                cond_to_entry.put(c.getCounter(), node);
                dist.set(c.getCounter(), 0f);
                open_list.set(c.getCounter(), true);
                closed.set(c.getCounter(), true);
            }
        }
        reacheable_conditions = 0;
        LinkedHashSet<GroundAction> temp_a;
        if (reacheability_setting) {
            temp_a = new LinkedHashSet(A);
        } else {
            temp_a = new LinkedHashSet(this.reachable);
        }
        final_achiever = new HashMap();

        HashMap<GroundAction, Float> action_to_cost = new HashMap();
        ArrayList<GroundAction> actions_for_complex_condition = new ArrayList();
        boolean first = true;
        Integer iteration = 0;
        while (!q.isEmpty() || first) {
            if (!first) {
                Conditions cn = q.removeMin().getData();
                closed.set(cn.getCounter(), true);
            }
            if (!all_paths) {
                Float goal_dist = this.check_goal_conditions(s, G, dist, closed);
                if (goal_dist != MAX_VALUE && !reacheability_setting) {
                    return goal_dist;
                }
            }

            Utils.dbg_print(debug, "Round" + iteration++);
            //trigger actions
//            Iterator<GroundAction> it = this.precondition_mapping.get(cn.getCounter()).iterator();
            first = false;
            Iterator<GroundAction> it = temp_a.iterator();

//            System.out.println("DEBUG: Actions investigated:"+this.precondition_mapping.get(cn.getCounter())+"\n starting from:"+cn);
            while (it.hasNext()) {//this can be made more efficient, of course, keeping track of the relantioship between
                //the condition just extracted and the action depending on it
                GroundAction gr = it.next();
                Float action_precondition_cost = this.compute_precondition_cost(s, dist, gr, closed);

                if (action_precondition_cost != MAX_VALUE) {
                    if (reacheability_setting) {
                        this.reachable.add(gr);
//                        gr.set_unit_cost(s);
                    }
                    action_to_cost.put(gr, action_precondition_cost);
                    //if (!gr.has_state_dependent_effects())
                    it.remove();//this can be removed since we are already looking for the closest/cheapest preconditions set
                    actions_for_complex_condition.add(gr);

                    //for (GroundAction gr : edges) {//this can be optimized a lot
                    Collection<Conditions> predicates = this.achieve.get(gr.counter);
                    Collection<Comparison> comparisons = this.possible_achievers.get(gr.counter);
                    for (Conditions p : predicates) {
                        if (closed.get(p.getCounter())) {
                            continue;
                        }
                        Float current_cost = action_to_cost.get(gr) + gr.getAction_cost();
                        update_cost_if_necessary(open_list, dist, (Conditions) p, q, cond_to_entry, current_cost);

                    }
                    for (Comparison comp : comparisons) {
                        if (closed.get(comp.getCounter())) {
                            continue;
                        }
                        if (!this.is_complex.get(comp.getCounter())) {
                            Float number_of_execution = null;
                            boolean super_simple_numeric_condition = (this.possible_achievers_inverted.get(comp.getCounter()).size() <= 1);
                            if ((this.additive_h && integer_actions) || super_simple_numeric_condition) {
                                number_of_execution = gr.getNumberOfExecutionInt(s, comp);
                            } else {
                                number_of_execution = gr.getNumberOfExecution(s, comp);
                            }
                            if (number_of_execution == Float.MAX_VALUE) {
                                continue;
                            }
                            Float action_cost = action_to_cost.get(gr);
                            if (!this.additive_h) {
                                Collection<GroundAction> all_achiever = this.possible_achievers_inverted.get(comp.getCounter());
                                //System.out.println(comp);
                                //System.out.println(all_achiever.size());
                                Float minimum = action_cost;
                                for (GroundAction ach : all_achiever) {
                                    if (action_to_cost.get(ach) != null) {
                                        Float ach_cost = action_to_cost.get(ach);
                                        if (ach_cost < minimum) {
                                            minimum = ach_cost;
                                        }
                                    }
                                }
                                action_cost = minimum;
                            }
                            //action_cost = 0;

                            Float current_cost = action_cost + number_of_execution * gr.getAction_cost();
//                        System.out.println(current_cost);
                            update_cost_if_necessary(open_list, dist, comp, q, cond_to_entry, current_cost);

                        } else {
                            Utils.dbg_print(debug, "interval based relaxation starting\n");
                            try {
                                Float current_cost = 1f;
                                if (this.additive_h) {
                                    current_cost = this.interval_based_relaxation_actions_with_cost(s, comp, actions_for_complex_condition, action_to_cost);
                                }
                                update_cost_if_necessary(open_list, dist, comp, q, cond_to_entry, current_cost);

                            } catch (CloneNotSupportedException ex) {
                                getLogger(Uniform_cost_search_H1.class.getName()).log(SEVERE, null, ex);
                            }
//                            catch (IOException ex) {
//                                Logger.getLogger(Uniform_cost_search_H1.class.getName()).log(Level.SEVERE, null, ex);
//                            }
                            Utils.dbg_print(debug, "interval based relaxation finished\n");

                        }
                    }
                }
            }

        }

        //System.out.println("Current Estimate to the goal:"+this.compute_float_cost(s, G, dist));
        return this.check_goal_conditions(s, G, dist, closed);
    }

    protected boolean generate_achievers() throws Exception {
        interact_with = new HashMap();
        achieve = new HashMap();
        possible_achievers = new HashMap();
        this.possible_achievers_inverted = new HashMap();
        achievers_inverted = new HashMap();
        precondition_mapping = new HashMap();

        //this should also include the indirect dependencies, otherwise does not work!!
        Set<GroundAction> useless_actions = new HashSet();
        for (GroundAction gr : this.A) {
            LinkedHashSet<Comparison> comparisons = new LinkedHashSet();
            LinkedHashSet<Comparison> reacheable_comparisons = new LinkedHashSet();
            LinkedHashSet<Conditions> literals = new LinkedHashSet();
            boolean at_least_one_service = false;
            for (Conditions c : this.all_conditions) {

                if (precondition_mapping.get(c.getCounter()) == null) {
                    precondition_mapping.put(c.getCounter(), new LinkedHashSet());
                }
                LinkedHashSet<GroundAction> action_list = new LinkedHashSet();
                if (c instanceof Comparison) {
//                    System.out.println("Condition under analysis:"+c);
//                    System.out.println("Counter is:"+c.getCounter());
                    Comparison comp = (Comparison) c;
                    if (comp.involve(gr.getNumericFluentAffected())) {
                        comparisons.add(comp);

                        if (this.is_complex.get(comp.getCounter())) {
                            at_least_one_service = true;
                            reacheable_comparisons.add(comp);
                        } else if (gr.is_possible_achiever_of(comp)) {
                            at_least_one_service = true;
                            reacheable_comparisons.add(comp);
                            action_list.add(gr);
                        }
                    }
                    if (this.possible_achievers_inverted.get(comp.getCounter()) == null) {
                        this.possible_achievers_inverted.put(comp.getCounter(), action_list);
                    } else {
                        LinkedHashSet<GroundAction> temp = this.possible_achievers_inverted.get(comp.getCounter());
                        temp.addAll(action_list);
                        this.possible_achievers_inverted.put(comp.getCounter(), temp);
                    }
                } else if (c instanceof Predicate) {
                    Predicate p = (Predicate) c;
                    if (gr.achieve(p)) {
                        at_least_one_service = true;
                        literals.add(p);
                        action_list.add(gr);
                    }
                    if (this.achievers_inverted.get(p.getCounter()) == null) {
                        this.achievers_inverted.put(p.getCounter(), action_list);
                    } else {
                        LinkedHashSet<GroundAction> temp = this.achievers_inverted.get(p.getCounter());
                        temp.addAll(action_list);
                        this.achievers_inverted.put(p.getCounter(), temp);
                    }

                } else if (c instanceof NotCond) {
                    NotCond c1 = (NotCond) c;
                    if (!c1.isTerminal()) {
                        System.out.println("Not formula has to be used as a terminal, and it is not:" + c1);
                        System.exit(-1);
                    }
                    Predicate p = (Predicate) c1.getSon();
                    if (gr.delete(p)) {
                        at_least_one_service = true;
                        literals.add(c1);
                        action_list.add(gr);
                    }
                    if (this.achievers_inverted.get(c1.getCounter()) == null) {
                        this.achievers_inverted.put(c1.getCounter(), action_list);
                    } else {
                        LinkedHashSet<GroundAction> temp = this.achievers_inverted.get(c1.getCounter());
                        temp.addAll(action_list);
                        this.achievers_inverted.put(c1.getCounter(), temp);
                    }

                }

                if (gr.preconditioned_on(c)) {//build mapping from atoms to actions
//                    System.out.println("Gr:"+ gr);
//                    try {
//                        System.in.read();
//                    } catch (IOException ex) {
//                        Logger.getLogger(Uniform_cost_search_H1.class.getName()).log(Level.SEVERE, null, ex);
//                    }

                    LinkedHashSet<GroundAction> temp = this.precondition_mapping.get(c.getCounter());
                    temp.add(gr);
                    this.precondition_mapping.put(c.getCounter(), temp);

                }

            }
//            if (at_least_one_service){
            achieve.put(gr.counter, literals);
            interact_with.put(gr.counter, comparisons);
            possible_achievers.put(gr.counter, reacheable_comparisons);
//            }else{
//                useless_actions.add(gr);
//            }
        }
//        boolean ret = !useless_actions.isEmpty();

//        A.removeAll(useless_actions);
        Utils.dbg_print(debug, "Identify complex achievers");

        for (Comparison comp : this.complex_condition_set) {
            HashSet<NumEffect> num_effects = new LinkedHashSet();
            HashMap<NumEffect, Boolean> temp_mark = new HashMap();
            HashMap<NumEffect, Boolean> per_mark = new HashMap();
            sorted_nodes = new LinkedList();
            for (GroundAction gr : A) {
                for (NumEffect nf : gr.getNumericEffectsAsCollection()) {
                    temp_mark.put(nf, false);
                    per_mark.put(nf, false);
                    num_effects.add(nf);
                }
            }
            for (NumEffect a : num_effects) {
                if ((!per_mark.get(a)) && (comp.getLeft().involve(a.getFluentAffected()))) {
                    visit(a, num_effects, temp_mark, per_mark, sorted_nodes);
                }
            }
            LinkedHashSet<GroundAction> action_list = new LinkedHashSet();
            for (GroundAction gr : A) {
                for (NumEffect neff : gr.getNumericEffectsAsCollection()) {

                    if (sorted_nodes.contains(neff)) {
                        possible_achievers.get(gr.counter).add(comp);
                        action_list.add(gr);
                    }

                }
            }
            if (this.possible_achievers_inverted.get(comp.getCounter()) == null) {
                this.possible_achievers_inverted.put(comp.getCounter(), action_list);
            } else {
                LinkedHashSet<GroundAction> temp = this.possible_achievers_inverted.get(comp.getCounter());
                temp.addAll(action_list);
                this.possible_achievers_inverted.put(comp.getCounter(), temp);
            }
            if (debug == 1) {
                System.out.println("Comparison:" + comp);
                System.out.println("Achievers Set" + this.possible_achievers_inverted.get(comp.getCounter()));
            }

        }
        Utils.dbg_print(debug, "Complex achievers identified");
        return false;//to fix at some point
    }

    private void update_cost_if_necessary(ArrayList<Boolean> open_list, ArrayList<Float> dist, Conditions p, FibonacciHeap<Conditions> q, HashMap<Integer, FibonacciHeapNode> cond_to_entry, Float current_cost) {
        if (current_cost == Float.MAX_VALUE) {
            return;
        }
        if (open_list.get(p.getCounter())) {
            if (dist.get(p.getCounter()) > current_cost) {
                q.decreaseKey(cond_to_entry.get(p.getCounter()), current_cost);
                dist.set(p.getCounter(), current_cost);
            }
        } else {
            dist.set(p.getCounter(), current_cost);
            open_list.set(p.getCounter(), true);
            FibonacciHeapNode node = new FibonacciHeapNode(p);
            q.insert(node, current_cost);
            cond_to_entry.put(p.getCounter(), node);
            reacheable_conditions++;
        }
    }

    protected void add_redundant_constraints() throws Exception {
        redundant_constraints = new HashMap();

        for (GroundAction a : A) {
            a.setPreconditions(a.getPreconditions().introduce_red_constraints());

        }

        G = G.introduce_red_constraints();
        //System.out.println(G.toString());
    }

    protected void compute_redundant_constraint(Set<Conditions> set) throws Exception {
        LinkedHashSet temp = new LinkedHashSet();
        ArrayList<Conditions> set_as_array = new ArrayList(set);
        int counter = 0;
        for (int i = 0; i < set_as_array.size(); i++) {
            for (int j = i + 1; j < set_as_array.size(); j++) {
                Conditions c_1 = set_as_array.get(i);
                Conditions c_2 = set_as_array.get(j);
                if ((c_1 instanceof Comparison) && (c_2 instanceof Comparison)) {
                    counter++;
                    Comparison a1 = (Comparison) c_1;
                    Comparison a2 = (Comparison) c_2;
                    ExtendedNormExpression lhs_a1 = (ExtendedNormExpression) a1.getLeft();
                    ExtendedNormExpression lhs_a2 = (ExtendedNormExpression) a2.getLeft();
                    ExtendedNormExpression expr = lhs_a1.sum(lhs_a2);
                    String new_comparator = ">=";
                    if (!a1.getComparator().equals(a2.getComparator())) {
                        new_comparator = ">=";
                    } else {
                        new_comparator = a1.getComparator();
                    }

                    Comparison newC = new Comparison(new_comparator);
                    newC.setLeft(expr);
                    newC.setRight(new ExtendedNormExpression(new Float(0.0)));
                    newC.normalize();

                    ExtendedNormExpression tempLeft = (ExtendedNormExpression) newC.getLeft();

                    if (tempLeft.summations.size() < 2) {
                        continue;
                    }
                    redundant_constraints.put(newC, true);
                    temp.add(newC);
                }
            }
        }
//        System.out.println("New conditions now:"+counter);
//        System.out.println("Set before:"+set.size());
        set.addAll(temp);
//        System.out.println("Set after:"+set.size());
    }

}

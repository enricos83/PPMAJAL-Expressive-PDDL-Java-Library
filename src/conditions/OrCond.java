/* 
 * Copyright (C) 2010-2017 Enrico Scala. Contact: enricos83@gmail.com.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package conditions;

import domain.Variable;
import expressions.NumEffect;
import expressions.NumFluent;
import heuristics.utils.achiever_set;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import problem.GroundAction;
import problem.PDDLObjects;
import problem.RelState;
import problem.State;

/**
 *
 * @author enrico
 */
public class OrCond extends Conditions {

    public HashSet son; // TODO: REMOVE?

    public OrCond() {
        super();
        sons = new LinkedHashSet();
    }

    @Override
    public String toString() {
        String ret_val = "Or(";
        for (Object o : sons) {
            ret_val = ret_val.concat(o.toString());
        }
        ret_val = ret_val.concat(")");

        return ret_val;
    }

    @Override
    public void normalize() {
        Iterator it = sons.iterator();
        while (it.hasNext()) {
            Object o = it.next();
            if (o instanceof Comparison) {
                Comparison comp = (Comparison) o;
                try {
//                    System.out.println(comp);
                    comp = comp.normalizeAndCopy();
                } catch (Exception ex) {
                    Logger.getLogger(OrCond.class.getName()).log(Level.SEVERE, null, ex);
                }
                if (comp == null) {
                    it.remove();
                }
            } else if (o instanceof AndCond) {
                AndCond temp = (AndCond) o;
                temp.normalize();
            } else if (o instanceof NotCond) {
                NotCond temp = (NotCond) o;
                temp.normalize();
            } else if (o instanceof OrCond) {
                OrCond temp = (OrCond) o;
                temp.normalize();
            }
        }

    }

    @Override
    public Conditions ground(Map<Variable, PDDLObject> substitution, PDDLObjects po) {
        OrCond ret = new OrCond();

        for (Object o : sons) {
            Conditions el = (Conditions) o;
            ret.sons.add(el.ground(substitution, po));
        }
        ret.grounded = true;
        return ret;
    }

    @Override
    public Conditions ground(Map substitution, int c) {
        Conditions ret = this.ground(substitution, null);
        ret.setCounter(c);
        return ret;
    }

    @Override
    public boolean eval(State s) {

        for (Object o : sons) {
            Conditions c = (Conditions) o;
            if (c.eval(s)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean isSatisfied(State s) {
        for (Object o : sons) {
            Conditions c = (Conditions) o;
            if (c.isSatisfied(s)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean can_be_true(RelState s) {
        for (Object o : sons) {
            Conditions c = (Conditions) o;
            if (c.can_be_true(s)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public void changeVar(Map substitution) {

        for (Object o : sons) {
            Conditions el = (Conditions) o;
            el.changeVar(substitution);
        }
    }

    @Override
    public Conditions clone() {
        OrCond ret = new OrCond();

//        ret.sons = new HashSet();
        ret.sons = (LinkedHashSet) this.sons.clone();
//        for(Object o: this.sons){
//            if (o instanceof AndCond){
//                AndCond a = (AndCond)o;
//                ret.sons.add(a.clone());
//            }else if(o instanceof NotCond){
//                NotCond a = (NotCond)o;
//                ret.sons.add(a.clone());
//            }else if(o instanceof OrCond){
//                OrCond a = (OrCond)o;
//                ret.sons.add(a.clone());
//            }else if(o instanceof Predicate){
//                Predicate a = (Predicate)o;
//                ret.sons.add(a.clone());            
//            }else if(o instanceof Comparison){
//                Comparison a = (Comparison)o;
//                ret.sons.add(a.clone());            
//            }else if(o instanceof Assigner){
//                Assigner a = (Assigner)o;
//                ret.sons.add(a.clone());            
//            }
//        }
        ret.grounded = this.grounded;
        return ret;
    }

    @Override
    public Conditions unGround(Map substitution) {
        OrCond ret = new OrCond();

        for (Object o : sons) {
            Conditions el = (Conditions) o;
            ret.sons.add(el.unGround(substitution));
        }
        ret.grounded = false;
        return ret;
    }

    @Override
    public boolean isUngroundVersionOf(Conditions conditions) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toSmtVariableString(int i) {
        String ret = "";

        //System.out.println(this);
        if (this.sons != null) {
            if (this.sons.size() > 1) {
                ret += "(or";
            }
            for (Object o : this.sons) {
                if (o instanceof Predicate) {
                    Predicate p = (Predicate) o;
                    ret += " " + p.toSmtVariableString(i);
                } else if (o instanceof NotCond) {
                    NotCond nc = (NotCond) o;
                    ret += " " + nc.toSmtVariableString(i);
                } else if (o instanceof Conditions) {
                    //System.out.println(o.getClass());
                    Conditions c = (Conditions) o;
                    ret += " " + c.toSmtVariableString(i);
                } else {
                    System.err.println("Not Supported" + o.getClass());
                }
            }
            if (this.sons.size() > 1) {
                ret += ")";
            }
        }
        return ret;
    }

    @Override
    public Set<NumFluent> getInvolvedFluents() {
        Set<NumFluent> ret = new HashSet();

        //System.out.println("Testing with:"+this.sons);
        if (this.sons != null) {
            for (Object o : this.sons) {
//                System.out.println("Testing with:"+o);
                if (o instanceof NumFluent) {
                    ret.add((NumFluent) o);
                } else if (o instanceof Conditions) {

                    Conditions c = (Conditions) o;
                    //System.out.println(c);
                    if (c.getInvolvedFluents() != null) {
                        ret.addAll(c.getInvolvedFluents());
                    }
                } else {
                    System.out.println("Error in getting involved fluents");
                }
            }
        }

        return ret;
    }

    public boolean involveReacheablePredicates(RelState possibleState) {

        if (this.sons != null) {
            for (Object o : this.sons) {
//                System.out.println("Testing with:"+o);
                if (o instanceof NotCond) {
                    NotCond nc = (NotCond) o;
                    if (nc.getSon() instanceof Predicate) {
                        Conditions p = (Conditions) nc.getSon();
                        if (possibleState.satisfy(p)) {
                            //System.out.println(p+" satisfiable");
                            return true;
                        } else {
                            //System.out.println(p);
                            return false;//System.out.println(";Predicate "+p+" is not possible");
                        }
                    }
                } else if (o instanceof Predicate) {
                    Predicate p = (Predicate) o;
                    if (possibleState.satisfy(p)) {
                        //System.out.println(p+" satisfiable");
                        return true;
                    } else {
                        //System.out.println(p);
                        return false;//System.out.println(";Predicate "+p+" is not possible");
                    }
                }
            }
        }
        return false;
    }

    public boolean involveReacheablePredicates(Collection<Predicate> possibleState) {

        if (this.sons != null) {
            for (Object o : this.sons) {
//                System.out.println("Testing with:"+o);
                if (o instanceof NotCond) {
                    NotCond nc = (NotCond) o;
                    if (nc.getSon() instanceof Predicate) {
                        Conditions p = (Conditions) nc.getSon();
                        if (possibleState.contains(p)) {
                            //System.out.println(p+" satisfiable");
                            return true;
                        } else {
                            //System.out.println(p);
                            return false;//System.out.println(";Predicate "+p+" is not possible");
                        }
                    }
                } else if (o instanceof Predicate) {
                    Predicate p = (Predicate) o;
                    if (possibleState.contains(p)) {
                        //System.out.println(p+" satisfiable");
                        return true;
                    } else {
                        //System.out.println(p);
                        return false;//System.out.println(";Predicate "+p+" is not possible");
                    }
                }
            }
        }
        return false;
    }

    @Override
    public Conditions weakEval(State s, HashMap invF) {
        LinkedHashSet to_keep = new LinkedHashSet();
        if (this.sons != null) {
            Iterator it = this.sons.iterator();
            while (it.hasNext()) {
                Object o2 = it.next();
                if (o2 instanceof Conditions) {
                    Conditions c = (Conditions) o2;
                    c.setFreeVarSemantic(this.freeVarSemantic);
//                    System.out.println(c);
                    c = c.weakEval(s, invF);
                    if (o2 instanceof PDDLObjectsEquality) {
                        if (c.isValid()) {
                            this.setValid(true);
                            this.setUnsatisfiable(false);
                            return this;
                        }
                    } else if (c.isValid()) {
                        this.setValid(true);
                        this.setUnsatisfiable(false);
                        return this;
                    } else if (c.isUnsatisfiable()) {
//                            to_remove.add(c);
                    } else {
                        to_keep.add(c);
                    }

                }
            }
        }
        this.sons = to_keep;

        return this;
    }

    @Override
    public String toSmtVariableString(int k, GroundAction gr, String var) {
        String ret = "";

        //System.out.println(this);
        if (this.sons != null) {
            if (this.sons.size() > 1) {
                ret += "(or";
            }
            for (Object o : this.sons) {
                if (o instanceof Predicate) {
                    Predicate p = (Predicate) o;
                    ret += " " + p.toSmtVariableString(k, gr, var);
                } else if (o instanceof NotCond) {
                    NotCond nc = (NotCond) o;
                    ret += " " + nc.toSmtVariableString(k, gr, var);
                } else if (o instanceof Conditions) {
                    //System.out.println(o.getClass());
                    Conditions c = (Conditions) o;
                    ret += " " + c.toSmtVariableString(k, gr, var);
                } else {
                    System.err.println("Not Supported" + o.getClass());
                }
            }
            if (this.sons.size() > 1) {
                ret += ")";
            }
        }
        return ret;
    }

    @Override
    public Conditions transform_equality() {
        if (this.sons == null) {
            return this;
        }
        OrCond ret = new OrCond();
        for (Conditions c1 : (Collection<Conditions>) this.sons) {
            ret.addConditions(c1.transform_equality());
        }
        return ret;
    }

    @Override
    public boolean is_affected_by(GroundAction gr) {
        if (this.sons != null && !this.sons.isEmpty()) {

            for (Conditions c : (Collection<Conditions>) this.sons) {
                if (c.is_affected_by(gr)) {
                    return true;
                }
            }

        }

        return false;
    }

    @Override
    public Conditions regress(GroundAction gr) {
        OrCond con = new OrCond();
        for (Object o : this.sons) {
            if (o instanceof Conditions) {
                Conditions t = (Conditions) o;
                Conditions temp = t.regress(gr);
                if (!temp.isValid()) {//needs to be satisfied
                    if (!temp.isUnsatisfiable()) {
                        if (temp instanceof OrCond) {
                            con.sons.addAll(((OrCond) temp).sons);
                        } else {
                            con.sons.add(temp);
                        }
                    }
                } else {
                    return new Predicate(Predicate.true_false.TRUE);
                }
            } else {
                System.out.println("AndCond: Condition " + o + " cannot be regressed");
                System.exit(-1);
            }
        }
        return con;
    }

    @Override
    public String pddlPrintWithExtraObject() {
        String ret_val = "(or ";
        for (Object o : sons) {
            if (o instanceof Conditions) {
                Conditions c = (Conditions) o;
                ret_val = ret_val.concat(c.pddlPrintWithExtraObject());
            } else if (o instanceof Comparison) {
                Comparison comp = (Comparison) o;
                ret_val = ret_val.concat(comp.pddlPrintWithExtraObject());
            } else {
                System.out.println("Error in pddlPrint:" + this);
                System.exit(-1);
            }
        }
        ret_val = ret_val.concat(")");
        return ret_val;
    }

    @Override
    public boolean can_be_false(RelState s) {
        for (Object o : sons) {
            Conditions c = (Conditions) o;
            if (!c.can_be_false(s)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public int hashCode() {
        final int sonHash = sons.hashCode();
        final int result = sonHash + 12;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (!(obj instanceof OrCond)) {
            return false;
        }

        final OrCond other = (OrCond) obj;

        if (!this.sons.equals(other.sons)) {
            return false;
        }

        return true;
    }

    @Override
    public void pddlPrint(boolean typeInformation, StringBuilder bui) {
        bui.append("(or ");
        for (Object o : sons) {
            if (o instanceof Conditions) {
                Conditions c = (Conditions) o;
                c.pddlPrint(typeInformation, bui);
            } else {
                System.out.println("Error in pddlPrint:" + this);
                System.exit(-1);
            }
        }
        bui.append(")");
    }

    @Override
    public void storeInvolvedVariables(Collection<Variable> vars) {
        if (this.sons != null) {
            for (Object o : this.sons) {
                if (o instanceof Conditions) {
                    Conditions c = (Conditions) o;
                    c.storeInvolvedVariables(vars);
                } else if (o instanceof NumEffect) {
                    NumEffect c = (NumEffect) o;
                    c.storeInvolvedVariables(vars);
                } else {
                    System.out.println("Error in getting involved variables");
                }
            }

        }
    }

    @Override
    public Set<Conditions> getTerminalConditions() {
        LinkedHashSet ret = new LinkedHashSet();
        if (this.sons == null) {
            return new LinkedHashSet();
        }
        for (Conditions c : (Collection<Conditions>) this.sons) {
            ret.addAll(c.getTerminalConditions());
        }
        return ret;
    }

    @Override
    public Float estimate_cost(ArrayList<Float> cond_dist, boolean additive_h) {
        if (this.sons == null) {
            return 0f;
        }
        Float ret = Float.MAX_VALUE;
        for (Conditions c : (Collection<Conditions>) this.sons) {
            if (c.estimate_cost(cond_dist, additive_h) != Float.MAX_VALUE) {
                ret = Math.min(c.estimate_cost(cond_dist, additive_h), ret);
            }
        }
        return ret;
    }

    @Override
    public achiever_set estimate_cost(ArrayList<Float> cond_dist, boolean additive_h, ArrayList<GroundAction> established_achiever) {
        achiever_set s = new achiever_set();
        s.cost = Float.MAX_VALUE;
        if (this.sons == null) {
            s.cost = 0f;
        } else {
            for (Conditions c : (Collection<Conditions>) this.sons) {
                achiever_set s1 = c.estimate_cost(cond_dist, additive_h, established_achiever);
                if (s1.cost != Float.MAX_VALUE) {
                    if (s.cost > s1.cost) {
                        s.actions = s1.actions;
                        s.cost = s1.cost;
                        s.target_cond.addAll(s1.target_cond);
                    }
                }
            }
        }
        return s;
    }

    @Override
    public Conditions push_not_to_terminals() {
        if (this.sons == null) {
            return this;
        }
        OrCond res = new OrCond();
        for (Conditions c : (Collection<Conditions>) this.sons) {
            c = c.push_not_to_terminals();
            res.addConditions(c);
        }

        return res;
    }

    @Override
    public Conditions and(Conditions precondition) {
        AndCond and = new AndCond();
        and.addConditions(precondition);
        and.addConditions(this);
        return and;
    }

    AndCond push_negation_demorgan() {
        AndCond res = new AndCond();
        for (Conditions c : (Collection<Conditions>) this.sons) {
            NotCond nc = new NotCond(c);
            res.addConditions(nc);
        }
        return res;
    }

    public boolean isSatisfied(RelState rs, ArrayList<Integer> dist, int i) {
        if (this.sons == null) {
            return true;
        }
        boolean ret = false;
        for (Conditions c : (Collection<Conditions>) this.sons) {
            if (c.isSatisfied(rs, dist, i)) {
                ret = true;
            }
        }
        return ret;
    }

    @Override
    public Conditions introduce_red_constraints() {
        if (this.sons == null) {
            return this;
        }
        OrCond ret = new OrCond();
        for (Conditions c : (Collection<Conditions>) this.sons) {
            ret.addConditions(c.introduce_red_constraints());
        }
        return ret;
    }

}
